<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get ('/','IndexController@getIndex');
Route::get ('bird/{id}', 'IndexController@getBird')->name('birdShow');
Route::get('birds','IndexController@getBirds');
Route::get('locations','IndexController@getLocations');
Route::get('tours','IndexController@getTours');
Route::get ('tour/{id}', 'IndexController@getTour')->name('tourShow');
Route::get('calendar','IndexController@getCalendar');
Route::get('news','IndexController@getNews');
Route::get('about','IndexController@getAbout');
Route::get('birds/add','IndexController@getAddBird');
Route::post('birds/add','IndexController@postAddBird')->name('birdStore');

Route::get('contact', function()
{
    return View::make('contact-page');
});

Route::post('contact','IndexController@postSaveMessage');


