@extends ('layouts.dbw')
@section ('content')
<style>
div.cliente{
	margin-top:10px;
	border: #cdcdcd medium solid;
	border-radius: 10px;
	-moz-border-radius: 10px;
	-webkit-border-radius: 10px;
}
#feature {

	background-color: #FDFEFE;

}
h1, h2, h3 {text-align: center;

	}

h1 {font-size: 80;}
</style>
<div id="feature">
		<h1>Dutch Bird Watch</h1>
		<h2>Your guide to watch birds in the Netherlands</h2>
</div>






<!-- <div class="row"> -->
<div class="grid">
  @foreach($birds as $bird_row => $bird)
    @if ((($bird_row + 1) % 3) === 1)
      <div class="row">
    @endif
    <div class="col-md-4" >
      <div class="cliente">
        <h2 class="title">
          <a href="{{ route('birdShow', ['id'=>$bird->id]) }}">{{ $bird->name_eng }}</a>
        </h2>
        <h4 align="center">Latin name: {{ $bird->name_lat }}</h4>
        <img src="{{ $bird->images }}" class="img-thumbnail" style="width:100%" >
        <div class="content">
          <p> Description: {{ $bird->description }} </p>
          <p> Season: <ul>@foreach($bird->season as $season1) <li>{{ $season1->season_name }}</li> @endforeach</ul></p>
          <p> Location: <ul>@foreach($bird->locations as $location) <li>{{ $location->location_name }}</li> @endforeach</ul></p>
          <a class="btn btn-default btn-block" href="{{ route('birdShow', ['id'=>$bird->id]) }}">View bird information</a>
        </div>
      </div>
    </div>
    @if (((($bird_row + 1) % 3) === 0) || (count($birds) === ($bird_row + 1)))
      </div>
    @endif
  @endforeach
</div>
<!-- </div> -->





































@endsection
