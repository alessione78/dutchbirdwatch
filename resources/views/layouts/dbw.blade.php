<!DOCTYPE html>
<html>
<head>
    <title>Dutch Bird Watch</title>
	
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    
    <style>

	body {
		background-image: url("{{ URL::to('/') }}/back.png");
                -moz-background-size: 100%; /* Firefox 3.6+ */
                -webkit-background-size: 100%; /* Safari 3.1+ & Chrome 4.0+ */
                -o-background-size: 100%; /* Opera 9.6+ */
                   background-size: 100%; /* modern browsers */
                background-repeat:no-repeat;
	}
        div.footer{
            background-color:lightgreen;
            margin:10px 0px;
            height: 35px;
            border-radius: 5px;
            clear: both;
            text-align: center;
            font-size: 100%;
            color:#fff;
            padding: 15px;
            line-height: 9px;
        }

        div.container{
            background-color:#7FB3D5;
        }

    </style>

</head>
<body>

        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <nav class="navbar navbar-default" style="margin-top: 15px;" >
                        <div class="container-fluid">
                            <div class="navbar-header">
				<a href="/" class="navbar-left"><img height="50" width="60"  alt="Dutch Bird Watch" src="{{ URL::to('/') }}/logo.png"></a>
                            </div>
                            <div id="navbar" class="collapse navbar-collapse">
                                <ul class="nav navbar-nav">
                                    <li><a href="{{ URL::to('/') }}/birds">BIRDS</a></li>
                                    <li><a href="{{ URL::to('/') }}/locations">LOCATIONS</a></li>
                                    <li><a href="{{ URL::to('/') }}/tours">TOURS</a></li>
                                    <li><a href="{{ URL::to('/') }}/calendar">CALENDAR</a></li>
                                    <li><a href="{{ URL::to('/') }}/news">NEWS</a></li>
                                    <li><a href="{{ URL::to('/') }}/about">About</a></li>
                                    <li><a href="{{ URL::to('/') }}/contact">Contact</a></li>
				    <li><a href="{{ URL::to('/') }}/#">Login</a></li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    @yield('content')
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                            <div class="footer">
                &#169; 2017 All Rights Reserved. DutchBirdWatch
            </div>
                </div>
            </div>
        </div>
           <script src="https://code.jquery.com/jquery-3.1.1.js"></script> 
            <!-- Latest compiled and minified JavaScript -->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
</body>
</html>
