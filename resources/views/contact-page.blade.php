@extends ('layouts.dbw')
@section ('content')
<div id="main" class="clear-block">
<link href=" {{ asset('_css/main.css') }}" rel="stylesheet" media="screen, projection">
<body id="blogPage">
<div id="part">
  <article id="mainContent">
    <h2>Contact us</h2>
    <article class="post">
      {{ Form::open(array('url'=> 'contact')) }}         
      {{ Form::label('email','E-mail address:') }}
      {{ Form::text('email') }}
      {{ Form::label('subject','Subject:') }}
      {{ Form::text('subject') }}            
      {{ Form::label('message','Message:') }}
      {{ Form::textarea('message',null,['size'=>'30x5']) }}
      {{ Form::submit('Send') }}
      {{ Form::close() }}   
    </article>
  </article>
</div>
</body>

</div>
@endsection
