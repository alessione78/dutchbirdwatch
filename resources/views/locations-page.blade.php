@extends ('layouts.dbw')
@section ('content')
<div id="main" class="clear-block">

    <style>
       #map {
        height: 400px;
        width: 100%;
       }
    </style>


    <h3>Google Maps</h3>
    <div id="map"></div>
    <script>
      function initMap() {
        var uluru = {lat: 51.9165855067814, lng: 4.4935253466796965};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxJh7ueW7CkALPcysydm7L49_OA5yiujI&callback=initMap">
    </script>

	<table class="table table-hover">
		<thead>
			<tr>
			<th>Location name</th>
			<th>Location description</th>
			</tr>
		<thead>
         @foreach($locations as $location)
		<tbody>
			<tr>
			<th>{{ $location->location_name }}</th>
			<th>{{ $location->location_description }}</th>
			</tr>
		</tbody>

	@endforeach
	</table>


@endsection
