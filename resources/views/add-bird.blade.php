@extends ('layouts.dbw')
@section ('content')
				<div id="main" class="clear-block">
	@if (count ($errors) >0)
	<div class="alert alert-danger">
	    <ul>
	    @foreach ($errors->all() as $error)
		<li>{{$error}}</li>
	    @endforeach
	    </ul>
	</div>
	@endif
						<h2 class="title">
							Here Add Bird to DB
						</h2>
				</div>
				
<div class="form">
<form method="post">
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />
	<div class="form-group">
	    <label for="name_eng">English name*</label>
	    <input type="text" class="form-control" name="name_eng" />
	</div>
	<div class="form-group">
	    <label for="name_lat">Latin name*</label>
	    <input type="text" class="form-control" name="name_lat" />
	</div>
	<div class="form-group">
	    <label for="season">Season (number)*</label>
	    <input type="text" class="form-control" name="season_id" />
	</div>
	<div class="form-group">
	    <label for="location">Location (number)*</label>
	    <input type="text" class="form-control" name="location_id" />
	</div>
	<div class="form-group">
	    <label for="tour">Tour*</label>
	    <input type="text" class="form-control" name="tour" />
	</div>
	<div class="form-group">
	    <label for="images">Images (url)</label>
	    <input type="text" class="form-control" name="images" />
	</div>
	<div class="form-group">
	    <label for="description">Description*</label>
	    <textarea class="form-control" name="description"></textarea>
	</div>
	<button type="submit">Save</button>
</form>
</div>
@endsection
