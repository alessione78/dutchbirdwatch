@extends ('layouts.dbw')
@section ('content')
				<div id="main" class="clear-block">
					@if($bird)
						<h2 class="title">Common name: {{ $bird->name_eng }}</h2>
						<h4>Latin name: {{ $bird->name_lat }}</h4>
						<img src="{{ $bird->images }}" class="img-thumbnail" style="width:100%" >

						<div class="content">
							<p> Description: {!! $bird->description !!} </p>
							<p> Season: <ul>@foreach($bird->season as $season1) <li>{{ $season1->season_name }}</li> @endforeach</ul> </p>
							<p> Location: <ul>@foreach($bird->locations as $location) <li>{{ $location->location_name }}</li> @endforeach</ul> </p>
						</div>
					@endif
					
					<a class="btn btn-default " href="{{ URL::to('/') }}">HOME</a>
				</div>
@endsection
