@extends ('layouts.dbw')
@section ('content')

<h2 class="title">Birds available for tours</h2>

	<table class="table table-hover">
		<thead>
			<tr>
			<th>Common name</th>
			<th>Latin name</th>
			<th>Image</th>
			<th>
			    <div class="dropdown">
			      <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Season*
			      <span class="caret"></span></button>
			      <ul class="dropdown-menu">
				@foreach($seasons as $season1)
				<li><a href="{{ $season1->name }}"> {{ $season1->season_name }}</a> </li>
				@endforeach
			      </ul>
			     </div>
			</th>
			<th>
			    <div class="dropdown">
			      <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Location*
			      <span class="caret"></span></button>
			      <ul class="dropdown-menu">
				@foreach($locations as $location1)
				<li><a href="{{ $location1->location_name }}"> {{ $location1->location_name }}</a> </li>
				@endforeach
				
			      </ul>
			     </div>
			</th>
			<th>
			    <div class="dropdown">
			      <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Tour*
			      <span class="caret"></span></button>
			      <ul class="dropdown-menu">
				@foreach($tours as $tour)
				<li><a href="{{ $tour->tour }}"> {{ $tour->tour }}</a> </li>
				@endforeach
				
			      </ul>
			     </div>
			</th>
			<th>select tour</th>
			</tr>
		<thead>
         @foreach($birds as $bird)
		<tbody>
			<tr>
			<th><a href="{{ route('birdShow', ['id'=>$bird->id]) }}">{{ $bird->name_eng }}</a></th>
			<th><a href="{{ route('birdShow', ['id'=>$bird->id]) }}">{{ $bird->name_lat }}</a></th>
			<th><img height="100" width="120" src="{{ $bird->images }}" class="img-thumbnail" ></th>
			<th> <ul>@foreach($bird->season as $season1) <li>{{ $season1->season_name }}</li> @endforeach</ul></th>
			<th><ul>@foreach($bird->locations as $location) <li>{{ $location->location_name }}</li> @endforeach</ul></th>
			<th>{{ $bird->tour }}</th>
			<th><input type="checkbox"></th>
			</tr>
		</tbody>

	@endforeach
	</table>
	

* - to be filtered
        
<br>

<a class="btn btn-default " href="#">Book selected tours</a>
<a class="btn btn-default " href="{{ URL::to('/') }}/birds/add">Add bird</a>
@endsection
