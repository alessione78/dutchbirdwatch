@extends ('layouts.dbw')
@section ('content')
<div id="main" class="clear-block">

<h2 class="title">tours</h2>



	<table class="table table-hover">
		<thead>
			<tr>
			<th>Tour name</th>
			<th>Start date</th>
			<th>End Date</th>
			<th>
			    <div class="dropdown">
			      <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Season*
			      <span class="caret"></span></button>
			      <ul class="dropdown-menu">
				@foreach($tours as $tour1)
				<li><a href="{{ route('tourShow', ['id'=>$tour1->id]) }}"> {{ $tour1->season_id }}</a> </li>
				@endforeach
			      </ul>
			     </div>
			</th>
			<th>Description</th>
			<th>
			    <div class="dropdown">
			      <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Available*
			      <span class="caret"></span></button>
			      <ul class="dropdown-menu">
				@foreach($tours as $tour1)
				<li><a href="{{ $tour1->available }}"> {{ $tour1->available }}</a> </li>
				@endforeach
				
			      </ul>
			     </div>
			</th>
			<th>select tour</th>
			</tr>
		<thead>
         @foreach($tours as $tour)
		<tbody>
			<tr>
			<th><a href="{{ route('tourShow', ['id'=>$tour->id]) }}">{{ $tour->tour_name }}</a></th>
			<th>{{ $tour->start_date }}</th>
			<th>{{ $tour->end_date }}</th>
			<th>{{ $tour->season_id }}</th>
			<th>{{ $tour->description }}</th>
			<th>{{ $tour->available }}</th>
			<th><input type="checkbox"></th>
			</tr>
		</tbody>

	@endforeach
	</table>



<a class="btn btn-default btn-block" href="/">Checkout</a>
@endsection
