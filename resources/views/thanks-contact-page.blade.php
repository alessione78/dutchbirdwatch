@extends ('layouts.dbw')
@section ('content')
<div id="main" class="clear-block">
<link href=" {{ asset('_css/main.css') }}" rel="stylesheet" media="screen, projection">
<body id="blogPage">
<div id="contentWrapper">
  <article id="mainContent">
    <h1>Email sent</h1>
    <article class="post">
      <h2>Thank you for contacting us</h2>
		<p>A copy of your message was sent to: {{ $theEmail }} </p>
    </article>
  </article>
</div>
</body>
</div>
@endsection
