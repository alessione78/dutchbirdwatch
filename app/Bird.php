<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bird extends Model
{
    protected $table = 'birds';
    protected $fillable = ['name_eng','name_lat','season_id','location_id','tour','description'];
    public $timestamps = false;

	//public function season() {
        //    return $this->belongsTo('App\Season');
	//}

	public function locations()
	{
		return $this->belongsToMany('App\Location', 'bird-location');
	}

	public function season()
	{
		return $this->belongsToMany('App\Season', 'bird-season');
	}
}
