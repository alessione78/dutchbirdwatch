<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    protected $table = 'seasons';
    protected $fillable = ['season_name','season_description'];
    public $timestamps = false;

    	//public function bird() {
        //return $this->belongsTo('App\Bird');
	//}
    	public function bird() {
        return $this->hasMany('App\Bird');
	}
}
