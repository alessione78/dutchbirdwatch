<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
	protected $table = 'emails';
	public $timestamps = false;
}

