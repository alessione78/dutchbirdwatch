<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    protected $table = 'tours';
    protected $fillable = ['tour_name','start_date','end_date', 'season_id', 'description', 'available'];
    public $timestamps = false;

    	public function bird() {
        return $this->hasMany('App\Bird');
	}
}
