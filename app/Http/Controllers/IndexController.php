<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bird;
use App\Season;
use App\Location;
use App\Tour;

use App\User;
use Illuminate\Support\Facades\Input;
use DateTime;
use View;
use Mail;

class IndexController extends Controller
{
    public function getIndex()  {
	$birds = Bird::all();
	return view('index_bird')->with(['birds' => $birds]);
    }

    public function getBird($id) {
	$bird = Bird::all()->where('id', $id)->first();
	return view('bird-page')->with(['bird'=>$bird]);
    }
	
    public function getBirds() {
	$birds = Bird::all();
	$seasons = Season::all();
	$locations = Location::all();
	$tours_tmp = Bird::select(['tour'])->get();
	$tours = $tours_tmp->unique('tour');
	$tours->values()->all();

	return view('birds-page')->with(['birds' => $birds, 'seasons' => $seasons, 'locations' => $locations, 'tours' => $tours]);

    }

	
    public function getLocations() {
	$locations = Location::all();
	return view('locations-page')->with(['locations' => $locations]);
    }

    public function getTours() {
	$tours = Tour::all();
	return view('tours-page')->with(['tours' => $tours]);
    }

    public function getTour($id) {
	$tour = Tour::all()->where('id', $id)->first();
	return view('tour-page')->with(['tour' => $tour]);
    }

    public function getCalendar() {
	return view('calendar-page');
    }

    public function getNews() {
	return view('news-page');
    }

    public function getAbout() {
	return view('about-page');
    }

    public function getContact() {
	return view('contact-page');
    }

public function postSaveMessage(Request $request)
	{ 
		if ($request->isMethod('POST'))
		{
			$data = array('email'=>Input::get('email'), 'message'=>Input::get('message'));
			
			$user = new User;
			$user->email = Input::get('email');
			$user->subject = Input::get('subject');
			$user->message = Input::get('message');			
			$user->date= new DateTime('now');
			$user->save();
			
			$theEmail = Input::get('email');       
			
			Mail::send(
            [],
            [],
            function($message) use ($data)
            {
                $message->setBody($data['message'], 'text/html');
                $message->from('info@dutchbirdwatch.tk');
                $message->subject('Copy of your message');
                $message->to( $data['email'] );
            }
        );	
        
        
			return View::make('thanks-contact-page')->with('theEmail', $theEmail);
		}
	}

    public function getAddBird() {
	return view('add-bird');
    }


    public function postAddBird(Request $request) {

	$this->validate($request, [
		'name_eng' => 'required|max:60',
		'name_lat' => 'required|unique:birds,name_lat',
		'season_id' => 'required',
		'location_id' => 'required',
		'tour' => 'required',
		'description' => 'required'

	]);

	$data = $request->all();
	$bird = Bird::create($data);
	return redirect('/');
    }
}
