<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'locations';
    protected $fillable = ['location_name','location_description','location_googlemap'];
    public $timestamps = false;

	public function bird() {
            return $this->hasMany('App\Bird');
 }
}
